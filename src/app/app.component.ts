import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
// import { Page12 } from '../pages/hcPage12/hcPage12';
// import { Page9 } from '../pages/hcPage9/hcPage9';
// import { Page8 } from '../pages/hcPage8/hcPage8';
// import { Page7 } from '../pages/hcPage7/hcPage7';
// import { Page6 } from '../pages/hcPage6/hcPage6';
// import { Page5 } from '../pages/hcPage5/hcPage5';
// import { Page4 } from '../pages/hcPage4/hcPage4';
// import { Page2 } from '../pages/hcPage2/hcPage2';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

