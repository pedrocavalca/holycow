import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
// import { File } from '@ionic-native/file';
// import { FileOpener } from '@ionic-native/file-opener';
// import { NativeAudio } from '@ionic-native/native-audio';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DragulaModule } from 'ng2-dragula';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomeIndexPage } from '../pages/homeIndex/homeIndex';
import { Credits } from '../pages/credits/credits';
import { Page1 } from '../pages/hcPage1/hcPage1';
import { Page2 } from '../pages/hcPage2/hcPage2';
import { Page3 } from '../pages/hcPage3/hcPage3';
import { Page4 } from '../pages/hcPage4/hcPage4';
import { Page5 } from '../pages/hcPage5/hcPage5';
import { Page6 } from '../pages/hcPage6/hcPage6';
import { Page7 } from '../pages/hcPage7/hcPage7';
import { Page8 } from '../pages/hcPage8/hcPage8';
import { Page9 } from '../pages/hcPage9/hcPage9';
import { Page10 } from '../pages/hcPage10/hcPage10';
import { Page11 } from '../pages/hcPage11/hcPage11';
import { Page12 } from '../pages/hcPage12/hcPage12';
import { Page13 } from '../pages/hcPage13/hcPage13';
import { UtilsProvider } from '../providers/utils/utils';
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeIndexPage,
    Credits,
    Page1,
    Page2,
    Page3,
    Page4,
    Page5,
    Page6,
    Page7,
    Page8,
    Page9,
    Page10,
    Page11,
    Page12,
    Page13
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    DragulaModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeIndexPage,
    Credits,
    Page1,
    Page2,
    Page3,
    Page4,
    Page5,
    Page6,
    Page7,
    Page8,
    Page9,
    Page10,
    Page11,
    Page12,
    Page13
  ],
  providers: [
    // File,
    // FileOpener,
    // NativeAudio,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UtilsProvider,
    SmartAudioProvider
  ]
})
export class AppModule {}
