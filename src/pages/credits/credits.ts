import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-credits',
  templateUrl: 'credits.html'
})
export class Credits {
  
  private animaBackPageSrc: string = "";

  constructor(
    public navCtrl: NavController, 
    public utils: UtilsProvider) {
    
  }

  ionViewWillEnter(){
    this.animaBackPageSrc = "";
  }
  
  ionViewDidEnter(){

    setTimeout(() => {
      this.animaBackPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  goToPreviousPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToPreviousPage(this.navCtrl);
  }

}
