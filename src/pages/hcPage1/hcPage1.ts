import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page2 } from '../hcPage2/hcPage2';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc1',
  templateUrl: 'hcPage1.html'
})
export class Page1 {
  @ViewChild("page1Reading") page1Reading;

  private assetPageFolder = "cena1/";
  private animaNextPageSrc: string = ""; 
  private animaVacaSrc: string = ""; 
  private animaSadhuSrc: string = ""; 
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider) {
      
  }

  ionViewWillEnter() {
    this.animaNextPageSrc = "";
    this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "india_vaca.gif"); 
    this.animaSadhuSrc = this.utils.imgPath(this.assetPageFolder, "india_sadhu.gif"); 
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewDidEnter() {
    this.smartAudios.loadAndPlay('page1Music', '01_india_sadhu.mp3', false, true);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('page1Music');
    this.page1Reading.nativeElement.pause();
    this.page1Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page2);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    // this.smartAudios.playReading('page1Reading');
    if (!this.isReading){
      console.log('playing', this.page1Reading);
      this.page1Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page1Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      console.log('paused');
      this.page1Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }
}
