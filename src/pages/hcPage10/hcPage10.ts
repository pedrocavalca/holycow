import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page11 } from '../hcPage11/hcPage11';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc10',
  templateUrl: 'hcPage10.html'
})
export class Page10 {
  @ViewChild("page10Reading") page10Reading;

  private assetPageFolder = "cena_boi/";
  private animaNextPageSrc: string = ""; 
  private animaBoiSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider, 
    private utils: UtilsProvider) {
    
  }

  ionViewWillEnter(){
    this.animaNextPageSrc = "";
    this.animaBoiSrc = this.utils.imgPath(this.assetPageFolder, "maranhao_boi_bumba.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewDidEnter(){
    this.smartAudios.loadAndPlay('batuque', 'batuque_festa.mp3', false, true);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('batuque');
    this.page10Reading.nativeElement.pause();
    this.page10Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page11);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page10Reading);
      this.page10Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page10Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page10Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

}
