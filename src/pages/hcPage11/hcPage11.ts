import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page12 } from '../hcPage12/hcPage12';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc11',
  templateUrl: 'hcPage11.html'
})
export class Page11 {
  @ViewChild("page11Reading") page11Reading;
  
  private assetPageFolder = "cena_vaca-boi/";
  private animaNextPageSrc: string = ""; 
  private animaVacaBoiSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider, 
    private utils: UtilsProvider) {
    
  }

  ionViewWillEnter(){
    this.animaNextPageSrc = "";
    this.animaVacaBoiSrc = this.utils.imgPath(this.assetPageFolder, "maranhao_frontal_vaca_boi.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }
  
  ionViewDidEnter(){
    this.smartAudios.loadAndPlay('batuque-vaca', 'batuque_festa.mp3', false, true);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('batuque-vaca');
    this.page11Reading.nativeElement.pause();
    this.page11Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page12);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page11Reading);
      this.page11Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page11Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page11Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

}
