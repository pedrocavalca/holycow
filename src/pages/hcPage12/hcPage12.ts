import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page13 } from '../hcPage13/hcPage13';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc12',
  templateUrl: 'hcPage12.html'
})
export class Page12 {
  @ViewChild("page12Reading") page12Reading;
  
  private assetPageFolder = "maquina_objetos/";
  private btnSrc: any = {};
  private animaVacaSrc: string = "";
  private animaMaquinaSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;
  private activity: any = {};

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider) {

      this.activity.keys = "BP";
      this.activity.usedKeys = "_";
      this.activity.displayText = "_ATO";
      this.activity.number = 1;
  }

  ionViewWillEnter() {
    this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-entrando.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
    this.btnSrc.V = this.utils.imgPath(this.assetPageFolder, "BT-V_up.png");
    this.btnSrc.F = this.utils.imgPath(this.assetPageFolder, "BT-F_up.png");
    this.btnSrc.B = this.utils.imgPath(this.assetPageFolder, "BT-B_up.png");
    this.btnSrc.P = this.utils.imgPath(this.assetPageFolder, "BT-P_up.png");
    this.smartAudios.preload('keyDown', 'digitacao.mp3', false, false);
    this.smartAudios.preload('BATO', 'maquina-de-palavras-mao-soco.mp3', false, false);
    this.smartAudios.preload('PATO', 'maquina-de-palavras-pato.mp3', false, false);
    this.smartAudios.preload('TABA', 'maquina-palavras-taba.mp3', false, false);
    this.smartAudios.preload('TAPA', 'maquina-de-palavras-mao-tapa.mp3', false, false);
    this.smartAudios.preload('FACA', 'maquina-palavras-faca.mp3', false, false);
    this.smartAudios.preload('VACA', 'maquina-palavras-boi.mp3', false, false);
    // this.smartAudios.preload('VIM', 'maquina-palavras-vim.mp3', false, false);
    this.smartAudios.preload('FIM', 'maquina-palavras-fim.mp3', false, false);
  }

  ionViewDidEnter(){
    
  }

  ionViewWillLeave() {
    this.smartAudios.unload('keyDown');
    this.page12Reading.nativeElement.pause();
    this.page12Reading.nativeElement.currentTime = 0;
    this.smartAudios.unload('BATO');
    this.smartAudios.unload('PATO');
    this.smartAudios.unload('TABA');
    this.smartAudios.unload('TAPA');
    this.smartAudios.unload('FACA');
    this.smartAudios.unload('VACA');
    // this.smartAudios.unload('VIM');
    this.smartAudios.unload('FIM');
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page13);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page12Reading);
      this.page12Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page12Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page12Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  btnPressed(letter: string){
    if (this.activity.keys.includes(letter)) {
      this.btnSrc[letter] = this.utils.imgPath(this.assetPageFolder, "BT-" + letter + "_down.png");
      this.smartAudios.play('keyDown');
    }
  }

  btnReleased(letter: string){
    this.btnSrc[letter] = this.utils.imgPath(this.assetPageFolder, "BT-" + letter + "_up.png");
    
    if (this.activity.keys.includes(letter)) {
      switch(this.activity.number) {
        case 1:
        case 2:
          if (letter === "B"){
            this.activity.displayText = "BATO";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Bato.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-susto.gif");
          } else if (letter === "P"){
            this.activity.displayText = "PATO";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Pato.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-dancando.gif");
          }
          break;
        case 3:
        case 4:
          if (letter === "B"){
            this.activity.displayText = "TABA";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Taba.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-dancando.gif");
          } else if (letter === "P"){
            this.activity.displayText = "TAPA";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-tapa.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-susto.gif");
          }
          break;
        case 5:
        case 6:
          if (letter === "F"){
            this.activity.displayText = "FACA";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Faca.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-susto.gif");
          } else if (letter === "V"){
            this.activity.displayText = "VACA";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-vaca.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-dancando.gif");
          }
          break;
        case 7:
        case 8:
          if (letter === "F"){
            this.activity.displayText = "FIM";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Fim.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-dancando.gif");
          } else if (letter === "V"){
            this.activity.displayText = "VIM";
            this.animaMaquinaSrc = this.utils.imgPath(this.assetPageFolder, "premio-Vim.gif");
            this.animaVacaSrc = this.utils.imgPath(this.assetPageFolder, "vaca-dancando.gif");
          }
          break;
      }
      if (this.activity.displayText !== "VIM"){
        this.smartAudios.play(this.activity.displayText);
      }
      this.activity.keys = "_";
      setTimeout(() => {
        this.activity.number++;
        switch (this.activity.number){
          case 2:
            if (this.activity.displayText === 'BATO'){
              this.activity.keys = "P";
            } else {
              this.activity.keys = "B";
            }
            this.activity.displayText = "_ATO"
            break;
          case 3:
            this.activity.keys = "BP";
            this.activity.displayText = "TA_A"
            break;
          case 4:
            if (this.activity.displayText === 'TABA'){
              this.activity.keys = "P";
            } else {
              this.activity.keys = "B";
            }
            this.activity.displayText = "TA_A"
            break;
          case 5:
            this.activity.keys = "FV";
            this.activity.displayText = "_ACA"
            break;
          case 6:
            if (this.activity.displayText === 'FACA'){
              this.activity.keys = "V";
            } else {
              this.activity.keys = "F";
            }
            this.activity.displayText = "_ACA"
            break;
          case 7:
            this.activity.keys = "FV";
            this.activity.displayText = "_IM"
            break;
          case 8:
            if (this.activity.displayText === 'FIM'){
              this.activity.keys = "V";
            } else {
              this.activity.keys = "F";
            }
            this.activity.displayText = "_IM"
            break;
          case 9:
            this.goToNextPage();
        }

      }, 3500);
    }
  }
}
