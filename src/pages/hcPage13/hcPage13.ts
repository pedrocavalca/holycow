import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';

@Component({
  selector: 'page-hc13',
  templateUrl: 'hcPage13.html'
})
export class Page13 {
  
  constructor(
    private navCtrl: NavController,
    private smartAudios: SmartAudioProvider) {
    
  }

  ionViewWillEnter() {
    // console.log('will enter homeIndex');
    this.smartAudios.preload('clickButton', 'clicar.mp3', false, false);
  }

  ionViewDidLeave() {
    // console.log('will leave homeIndex');
    this.smartAudios.unload('clickButton');
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.smartAudios.play('clickButton');
    this.navCtrl.popToRoot();
  }

}
