import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ISubscription } from "rxjs/Subscription";
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { Page3 } from '../hcPage3/hcPage3';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hcPage2',
  templateUrl: 'hcPage2.html'
})
export class Page2 implements OnDestroy {
  @ViewChild("page2Reading") page2Reading;

  private assetPageFolder = "cena2/";
  private animaOmSrc: string = "";
  private animaChuvaFacaSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;
  private q1 = [];
  private q2 = [];
  private dropSubscription: ISubscription;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider,
    private dragulaService: DragulaService) {
    
      this.dropSubscription = dragulaService.drop.subscribe((value) => {
        console.log(`drop: ${value[0]}`);
        this.onDrop(value.slice(1));
      });
  }

  resetActivity(){
    this.q1 = [];
    this.q2 = [];
    this.q1.push({
      id:'container1',
      src:this.utils.assetsImgsPath + this.assetPageFolder + "letter-V.png"
    });
    this.q1.push({
      id:'container2',
      src:this.utils.assetsImgsPath + this.assetPageFolder + "letter-F.png"
    });
  }

  ionViewWillEnter(){
    this.resetActivity();
    // console.log('q1', this.q1);
    // console.log('q2', this.q2);
    this.animaOmSrc = this.utils.imgPath(this.assetPageFolder, "om-close-cena2-tiny.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
    this.smartAudios.preload('clickLetter', 'clicar.mp3', false, false);
    this.smartAudios.preload('chuvaFaca', 'chuva_facas.mp3', false, true);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('clickLetter');
    this.smartAudios.unload('chuvaFaca');
    this.page2Reading.nativeElement.pause();
    this.page2Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page3);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page2Reading);
      this.page2Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page2Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page2Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  letterClicking(){
    // console.log('clicking letter')
    this.smartAudios.play('clickLetter');
  }

  private onDrop(args) {
    this.smartAudios.play('clickLetter');
    // console.log('args', args);
    let [el, target, source] = args;
    // console.log('element', el);
    // console.log('target', target);
    // console.log('source', source);
    if (el.id === 'container1' && target.id === "container3"){
      setTimeout(() => {
        this.goToNextPage(); 
      }, 500);
    } else if (el.id === 'container2' && target.id === "container3"){
      this.animaChuvaFacaSrc = this.utils.imgPath(this.assetPageFolder, "chuva-de-facas.gif");
      this.smartAudios.play('chuvaFaca');
      setTimeout(() => {
          // console.log('chuva de facas');
          this.animaChuvaFacaSrc = null;
          this.resetActivity();
       }, 2500);
    }
  }

  ngOnDestroy() {
    this.dropSubscription.unsubscribe();
    this.dragulaService.destroy('bag');
  }

}
