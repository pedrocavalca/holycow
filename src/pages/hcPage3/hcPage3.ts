import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page4 } from '../hcPage4/hcPage4';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hcPage3',
  templateUrl: 'hcPage3.html'
})
export class Page3 {
  @ViewChild("page3Reading") page3Reading;

  private assetPageFolder = "cena_faca/";
  private animaNextPageSrc: string = ""; 
  private animaFacaSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider) {
    
  }

  ionViewWillEnter() {
    this.animaNextPageSrc = "";
    this.animaFacaSrc = this.utils.imgPath(this.assetPageFolder, "faca.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewDidEnter() {
    this.smartAudios.loadAndPlay('page3Faca', '03_faca_003.mp3', false, false);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('page3Faca');
    this.page3Reading.nativeElement.pause();
    this.page3Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page4);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    // this.smartAudios.playReading('page1Reading');
    if (!this.isReading){
      // console.log('playing', this.page2Reading);
      this.page3Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page3Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page3Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

}
