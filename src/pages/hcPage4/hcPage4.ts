import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ISubscription } from "rxjs/Subscription";
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { Page5 } from '../hcPage5/hcPage5';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc4',
  templateUrl: 'hcPage4.html'
})
export class Page4 implements OnDestroy {
  @ViewChild("page4Reading1") page4Reading1;
  @ViewChild("page4Reading2") page4Reading2;

  private assetPageFolder = "cena_nepal_monte/";
  private isFirstReading: boolean = true;
  private isReading: boolean = false;
  private isCompleted: boolean = false;
  private animaOmSrc: string = "";
  private dragOmImageSrc: string = "";
  private btnAudioSrc: string = "";
  private q1 = [];
  private q2 = [];
  private dropSubscription: ISubscription;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider, 
    private dragulaService: DragulaService) {

      this.dropSubscription = dragulaService.drop.subscribe((value) => {
        // console.log(`drop: ${value[0]}`);
        this.onDrop(value.slice(1));
      });
  }

  private onDrop(args) {
    let [el, target, source] = args;
    if (target.id === "container2"){
      setTimeout(() => {
        this.goToNextPage();
       }, 500);
    }
  }

  ionViewWillEnter() {
    this.isFirstReading = true;
    this.isCompleted = false;
    this.q1 = [];
    this.q2 = [];
    this.q1.push("container-item");
    // console.log('q1', this.q1);
    // console.log('q2', this.q2);
    this.animaOmSrc = this.utils.imgPath(this.assetPageFolder, "animacao_om_nepal.gif");
    this.dragOmImageSrc = this.utils.imgPath(this.assetPageFolder, "om_idle.png");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
    this.smartAudios.preload('clickOm', 'clicar.mp3', false, false);
  }

  ionViewDidEnter(){
    this.smartAudios.loadAndPlay('omWalking', 'om_andando_viradas_cabeca.mp3', false, true);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('omWalking');
    this.page4Reading1.nativeElement.pause();
    this.page4Reading1.nativeElement.currentTime = 0;
    this.page4Reading2.nativeElement.pause();
    this.page4Reading2.nativeElement.currentTime = 0;
    this.smartAudios.unload('clickOm');
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page5);
  }

  next(){
    this.page4Reading1.nativeElement.pause();
    this.audioStopped();
    this.smartAudios.stop('omWalking');
    this.smartAudios.play('clickOm');
    this.isFirstReading = false;
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (this.isFirstReading){
      if (!this.isReading){
        // console.log('playing', this.page2Reading);
        this.page4Reading1.nativeElement.play();
        this.btnAudioSrc = this.utils.btAudioImg(true); 
        this.isReading = true;
        this.page4Reading1.nativeElement.onended = () => {
          this.audioStopped();
        }
      } else {
        // console.log('paused');
        this.page4Reading1.nativeElement.pause();
        this.audioStopped();
      }
    } else {
      if (!this.isReading){
        // console.log('playing', this.page2Reading);
        this.page4Reading2.nativeElement.play();
        this.btnAudioSrc = this.utils.btAudioImg(true); 
        this.isReading = true;
        this.page4Reading2.nativeElement.onended = () => {
          this.audioStopped();
        }
      } else {
        // console.log('paused');
        this.page4Reading2.nativeElement.pause();
        this.audioStopped();
      }
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  willDragOm() {
    // console.log("will drag om");
    this.smartAudios.play('clickOm');
    this.dragOmImageSrc = this.utils.imgPath(this.assetPageFolder, "animacao_click_om_nepal.gif");
  }

  droppedOm() {
    // console.log("dropped om");
    this.smartAudios.play('clickOm');
    this.dragOmImageSrc = this.utils.imgPath(this.assetPageFolder, "om_idle.png");
  }

  ngOnDestroy() {
    this.dropSubscription.unsubscribe();
    this.dragulaService.destroy('first-bag');
  }

}
