import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page6 } from '../hcPage6/hcPage6';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc5',
  templateUrl: 'hcPage5.html'
})
export class Page5 {
  @ViewChild("page5Reading") page5Reading;

  private assetPageFolder = "cena_bilhete-nepal/";
  private animaNextPageSrc: string = ""; 
  private animaOmReadingSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider) {
    
  }

  ionViewWillEnter() {
    this.animaNextPageSrc = "";
    this.animaOmReadingSrc = this.utils.imgPath(this.assetPageFolder, "close-nepal-om.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.page5Reading.nativeElement.pause();
    this.page5Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page6);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page5Reading);
      this.page5Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page5Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page5Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

}
