import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ISubscription } from "rxjs/Subscription";
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { Page7 } from '../hcPage7/hcPage7';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc6',
  templateUrl: 'hcPage6.html'
})
export class Page6 implements OnDestroy {
  @ViewChild("page6Reading") page6Reading;
  
  private assetPageFolder = "mochila_objetos/";
  private animaNextPageSrc: string = ""; 
  private btnAudioSrc: string = "";
  private isReading: boolean = false;
  private omImageSrc = null;
  private q1 = [];
  private q2 = [];
  private q3 = [];
  private lblObj1Show = false;
  private lblObj2Show = false;
  private lblObj3Show = false;
  private lblObj4Show = false;
  private mochila1Errado = false;
  private mochila2Errado = false;
  private dropSubscription: ISubscription;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider,
    private dragulaService: DragulaService) {
    
      dragulaService.setOptions('bag-items', {
        moves: function (el, container, handle) {
          // console.log('moves');
          // console.log('el', el);
          // console.log('container', container);
          // console.log('handle', handle);
          return handle.className.includes('objeto');
        }
      });
      
  }

  ionViewWillEnter(){
    this.dropSubscription = this.dragulaService.drop.subscribe((value) => {
      console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
    });
    this.q1 = [];
    this.q2 = [];
    this.q3 = [];
    this.q1.push({
      id:'obj1',
      class: 'objeto obj1',
      name:'mapa',
      src:this.utils.imgPath(this.assetPageFolder, "mapa.png")
    });
    this.q1.push({
      id:'obj2',
      class: 'objeto obj2',
      name:'picareta',
      src:this.utils.imgPath(this.assetPageFolder, "picareta.png")
    });
    this.q1.push({
      id:'obj3',
      class: 'objeto obj3',
      name:'bota',
      src:this.utils.imgPath(this.assetPageFolder, "bota.png")
    });
    this.q1.push({
      id:'obj4',
      class: 'objeto obj4',
      name:'binoculo',
      src:this.utils.imgPath(this.assetPageFolder, "binoculo.png")
    });
    
    this.smartAudios.preload('clickObjTela6', 'clicar.mp3', false, false);
    this.smartAudios.preload('mapa', 'mapa.mp3', false, false);
    this.smartAudios.preload('picareta', 'picareta.mp3', false, false);
    this.smartAudios.preload('bota', 'bota.mp3', false, false);
    this.smartAudios.preload('binoculo', 'binoculo.mp3', false, false);
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();   
  }

  ionViewWillLeave() {
    this.smartAudios.unload('clickObjTela6');
    this.smartAudios.unload('mapa');
    this.smartAudios.unload('picareta');
    this.smartAudios.unload('bota');
    this.smartAudios.unload('binoculo');
    this.page6Reading.nativeElement.pause();
    this.page6Reading.nativeElement.currentTime = 0;
    this.dropSubscription.unsubscribe();
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page7);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page6Reading);
      this.page6Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page6Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page6Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  audioItem(itemName){
    console.log('audio item: ' + itemName);
    this.smartAudios.play(itemName);
  }

  objClicking(){
    // console.log('clicking letter')
    this.smartAudios.play('clickObjTela6');
  }

  private mochilaErrada(mochilaErrada, objId){
    setTimeout(() => {
      if (mochilaErrada === 'mochila1'){
        this.mochila1Errado = false;
        let obj = this.q2.find((objeto) => {
          return objeto.id === objId;
        });
        let index = this.q2.indexOf(obj);
        if(index>=0){
          this.q2.splice(index, 1);
        }
      } else if (mochilaErrada === 'mochila2'){
        this.mochila2Errado = false;
        let obj = this.q3.find((objeto) => {
          return objeto.id === objId;
        });
        let index = this.q3.indexOf(obj);
        console.log('index', index);
        if(index>=0){
          this.q3.splice(index, 1);
        }
      }
     }, 2000);
  }

  private mochilaCerta() {
    setTimeout(() => {
      this.omImageSrc = null;
      if (this.q1.length === 0){
        setTimeout(() => {
          this.animaNextPageSrc = this.utils.bgNextPage();
        }, 500);
      }
     }, 1500);
  }

  private onDrop(args) {
    this.smartAudios.play('clickObjTela6');
    let [el, target, source] = args;
    // do something
    console.log('element', el);
    console.log('target', target);
    console.log('source', source);
    let obj = this.q1.find((objeto) => {
      return objeto.id === el.id;
    });
    switch (el.id){
      case 'obj1':
        if (target.id === 'mochila1'){
          let obj = {
            id:'obj1',
            class: 'objeto obj1',
            name:'mapa',
            src:this.utils.imgPath(this.assetPageFolder, "mapa.png")
          };
          this.q1.push(obj);
          this.mochila1Errado = true;
          this.mochilaErrada('mochila1', obj.id);
        } else if (target.id === 'mochila2'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-mapa.png');
          obj.isCorrect = true;
          this.lblObj1Show = true;
          this.mochilaCerta();
        }
        break;
      case 'obj2':
        if (target.id === 'mochila1'){
          let obj = {
            id:'obj2',
            class: 'objeto obj2',
            name:'picareta',
            src:this.utils.imgPath(this.assetPageFolder, "picareta.png")
          };
          this.q1.push(obj);
          this.mochila1Errado = true;
          this.mochilaErrada('mochila1', obj.id);
        } else if (target.id === 'mochila2'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-picareta.png');
          obj.isCorrect = true;
          this.lblObj2Show = true;
          this.mochilaCerta();
        }
        break;
      case 'obj3':
        if (target.id === 'mochila1'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-bota.png');
          obj.isCorrect = true;
          this.lblObj3Show = true;
          this.mochilaCerta();
        } else if (target.id === 'mochila2'){
          let obj = {
            id:'obj3',
            class: 'objeto obj3',
            name:'bota',
            src:this.utils.imgPath(this.assetPageFolder, "bota.png")
          };
          this.q1.push(obj);
          this.mochila2Errado = true;
          this.mochilaErrada('mochila2', obj.id);
        }
        break;
      case 'obj4':
        if (target.id === 'mochila1'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-binoculo.png');
          obj.isCorrect = true;
          this.lblObj4Show = true;
          this.mochilaCerta();
        } else if (target.id === 'mochila2'){
          let obj = {
            id:'obj4',
            class: 'objeto obj4',
            name:'binoculo',
            src:this.utils.imgPath(this.assetPageFolder, "binoculo.png")
          };
          this.q1.push(obj);
          this.mochila2Errado = true;
          this.mochilaErrada('mochila2', obj.id);
        }
        break;
    }
  }


  ngOnDestroy() {
    this.dropSubscription.unsubscribe();
    this.dragulaService.destroy('bag-items');
  }

}
