import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ISubscription } from "rxjs/Subscription";
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { Page8 } from '../hcPage8/hcPage8';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc7',
  templateUrl: 'hcPage7.html'
})
export class Page7 implements OnDestroy {
  @ViewChild("page7Reading") page7Reading;
  
  private assetPageFolder = "mochila_objetos/";
  private animaNextPageSrc: string = ""; 
  private btnAudioSrc: string = "";
  private isReading: boolean = false;
  private omImageSrc = "";
  private q1 = [];
  private q2 = [];
  private q3 = [];
  private lblObj1Show = false;
  private lblObj2Show = false;
  private lblObj3Show = false;
  private lblObj4Show = false;
  private mochila1Errado = false;
  private mochila2Errado = false;
  private dropSubscription: ISubscription;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider,
    private dragulaService: DragulaService) {
    
      dragulaService.setOptions('bag2-items', {
        moves: function (el, container, handle) {
          // console.log('moves');
          // console.log('el', el);
          // console.log('container', container);
          // console.log('handle', handle);
          return handle.className.includes('objeto');
        }
      });
  }

  ionViewWillEnter(){
    this.dropSubscription = this.dragulaService.drop.subscribe((value) => {
      // console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
    });
    this.mochila1Errado = false;
    this.mochila2Errado = false;
    this.omImageSrc = "";
    this.q1 = [];
    this.q2 = [];
    this.q3 = [];
    this.q1.push({
      id:'obj1',
      class: 'objeto obj1',
      name:'faca',
      src:this.utils.imgPath(this.assetPageFolder, "faca.png")
    });
    this.q1.push({
      id:'obj2',
      class: 'objeto obj2',
      name:'violao',
      src:this.utils.imgPath(this.assetPageFolder, "viola.png")
    });
    this.q1.push({
      id:'obj3',
      class: 'objeto obj3',
      name:'farolete',
      src:this.utils.imgPath(this.assetPageFolder, "farolete.png")
    });
    this.q1.push({
      id:'obj4',
      class: 'objeto obj4',
      name:'vara',
      src:this.utils.imgPath(this.assetPageFolder, "vara.png")
    });
    // console.log('q1', this.q1);
    this.smartAudios.preload('clickObjTela7', 'clicar.mp3', false, false);
    this.smartAudios.preload('faca', 'faca.mp3', false, false);
    this.smartAudios.preload('violao', 'violao.mp3', false, false);
    this.smartAudios.preload('farolete', 'farolete.mp3', false, false);
    this.smartAudios.preload('vara', 'vara.mp3', false, false);
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewWillLeave() {
    this.smartAudios.unload('clickObjTela7');
    this.smartAudios.unload('faca');
    this.smartAudios.unload('violao');
    this.smartAudios.unload('farolete');
    this.smartAudios.unload('vara');
    this.page7Reading.nativeElement.pause();
    this.page7Reading.nativeElement.currentTime = 0;
    this.dropSubscription.unsubscribe();
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page8);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page7Reading);
      this.page7Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page7Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page7Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  audioItem(itemName){
    console.log('audio item: ' + itemName);
    this.smartAudios.play(itemName);
  }

  objClicking(){
    // console.log('clicking letter')
    this.smartAudios.play('clickObjTela7');
  }

  private mochilaErrada(mochilaErrada, objId){
    setTimeout(() => {
      if (mochilaErrada === 'mochila1'){
        this.mochila1Errado = false;
        let obj = this.q2.find((objeto) => {
          return objeto.id === objId;
        });
        let index = this.q2.indexOf(obj);
        if(index>=0){
          this.q2.splice(index, 1);
        }
      } else if (mochilaErrada === 'mochila2'){
        this.mochila2Errado = false;
        let obj = this.q3.find((objeto) => {
          return objeto.id === objId;
        });
        let index = this.q3.indexOf(obj);
        console.log('index', index);
        if(index>=0){
          this.q3.splice(index, 1);
        }
      }
     }, 2000);
  }

  private mochilaCerta() {
    setTimeout(() => {
      this.omImageSrc = null;
      if (this.q1.length === 0){
        setTimeout(() => {
          this.animaNextPageSrc = this.utils.bgNextPage();
        }, 500);
      }
     }, 1500);
  }

  private onDrop(args) {
    this.smartAudios.play('clickObjTela7');
    let [el, target, source] = args;
    // do something
    // console.log('element', el);
    // console.log('target', target);
    // console.log('source', source);
    // console.log('q1', this.q1);
    // console.log('q2', this.q2);
    // console.log('q3', this.q3);
    let obj = this.q1.find((objeto) => {
      return objeto.id === el.id;
    });
    switch (el.id){
      case 'obj1':
        if (target.id === 'mochila1'){
          // console.log('objeto certo!');
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-faca.png');
          obj.isCorrect = true;
          this.lblObj1Show = true;
          this.mochilaCerta();
        } else if (target.id === 'mochila2'){
          // console.log('objeto errada!');
          let obj = {
            id:'obj1',
            class: 'objeto obj1',
            name:'faca',
            src:this.utils.imgPath(this.assetPageFolder, "faca.png")
          };
          this.q1.push(obj);
          this.mochila2Errado = true;
          this.mochilaErrada('mochila2', obj.id);
        }
        break;
      case 'obj2':
        if (target.id === 'mochila1'){
          let obj = {
            id:'obj2',
            class: 'objeto obj2',
            name:'violao',
            src:this.utils.imgPath(this.assetPageFolder, "viola.png")
          };
          this.q1.push(obj);
          this.mochila1Errado = true;
          this.mochilaErrada('mochila1', obj.id);
        } else if (target.id === 'mochila2'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-violao.png');
          obj.isCorrect = true;
          this.lblObj2Show = true;
          this.mochilaCerta();
        }
        break;
      case 'obj3':
        if (target.id === 'mochila1'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-farolete.png');
          obj.isCorrect = true;
          this.lblObj3Show = true;
          this.mochilaCerta();
        } else if (target.id === 'mochila2'){
          let obj = {
            id:'obj3',
            class: 'objeto obj3',
            name:'farolete',
            src:this.utils.imgPath(this.assetPageFolder, "farolete.png")
          };
          this.q1.push(obj);
          this.mochila2Errado = true;
          this.mochilaErrada('mochila2', obj.id);
        }
        break;
      case 'obj4':
        if (target.id === 'mochila1'){
          let obj = {
            id:'obj4',
            class: 'objeto obj4',
            name:'vara',
            src:this.utils.imgPath(this.assetPageFolder, "vara.png")
          };
          this.q1.push(obj);
          this.mochila1Errado = true;
          this.mochilaErrada('mochila1', obj.id);
        } else if (target.id === 'mochila2'){
          this.omImageSrc = this.utils.imgPath(this.assetPageFolder, 'om-vara.png');
          obj.isCorrect = true;
          this.lblObj4Show = true;
          this.mochilaCerta();
        }
        break;
    }
  }

  ngOnDestroy() {
    // this.dragSubscription.unsubscribe();
    this.dropSubscription.unsubscribe();
    this.dragulaService.destroy('bag2-items');
  }

}
