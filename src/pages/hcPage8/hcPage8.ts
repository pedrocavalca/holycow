import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page9 } from '../hcPage9/hcPage9';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc8',
  templateUrl: 'hcPage8.html'
})
export class Page8 {
  @ViewChild("page8Reading") page8Reading;
  @ViewChild("bilheteReading") bilheteReading;

  private assetPageFolder = "bilhete_maquina/";
  private bgLetterSrc = null;
  private animaNextPageSrc: string = ""; 
  private btnAudioSrc: string = "";
  private btnAudioBilheteSrc: string = "";
  private isReading: boolean = false;
  private isReadingBilhete: boolean = false;
  private btnSrc: any = {};
  private answers: any = {}

  constructor(
    private navCtrl: NavController,
    private smartAudios: SmartAudioProvider,  
    private utils: UtilsProvider) {

      this.answers.voces = "_";
      this.answers.precisam = "_";
      this.answers.preocupar1 = "_";
      this.answers.preocupar2 = "_";
      this.answers.comigo = "_";
      this.answers.estou = "_";
      this.answers.bem = "_";
      this.answers.fui = "_";
      this.answers.encontrar1 = "_";
      this.answers.encontrar2 = "_";
      this.answers.grande1 = "_";
      this.answers.grande2 = "_";
      this.answers.bumba = "_";
      this.answers.boi = "_";
      this.answers.brasil = "_";
      this.answers.field = "voces";
  }

  ionViewWillEnter() {
    this.animaNextPageSrc = "";
    this.bgLetterSrc = this.utils.imgPath(this.assetPageFolder, "bgLetter.png");
    this.btnSrc.V = this.utils.imgPath(this.assetPageFolder, "BT-V.png");
    this.btnSrc.P = this.utils.imgPath(this.assetPageFolder, "BT-P.png");
    this.btnSrc.G = this.utils.imgPath(this.assetPageFolder, "BT-G.png");
    this.btnSrc.D = this.utils.imgPath(this.assetPageFolder, "BT-D.png");
    this.btnSrc.C = this.utils.imgPath(this.assetPageFolder, "BT-C.png");
    this.btnSrc.T = this.utils.imgPath(this.assetPageFolder, "BT-T.png");
    this.btnSrc.B = this.utils.imgPath(this.assetPageFolder, "BT-B.png");
    this.btnSrc.F = this.utils.imgPath(this.assetPageFolder, "BT-F.png");
    this.smartAudios.preload('keyPressed', 'digitacao.mp3', false, false);
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.btnAudioBilheteSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
    this.audioBilheteStopped();
  }

  ionViewDidEnter(){
    
  }

  ionViewWillLeave() {
    this.smartAudios.unload('keyPressed');
    this.page8Reading.nativeElement.pause();
    this.page8Reading.nativeElement.currentTime = 0;
    this.bilheteReading.nativeElement.pause();
    this.bilheteReading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page9);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page8Reading);
      this.page8Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page8Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page8Reading.nativeElement.pause();
      this.audioStopped();
    }
  }
  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

  audioHelp(){
    // console.log('Reading Audio');
    if (!this.isReadingBilhete){
      // console.log('playing', this.bilheteReading);
      this.bilheteReading.nativeElement.play();
      this.btnAudioBilheteSrc = this.utils.btAudioImg(true); 
      this.isReadingBilhete = true;
      this.bilheteReading.nativeElement.onended = () => {
        this.audioBilheteStopped();
      }
    } else {
      // console.log('paused');
      this.bilheteReading.nativeElement.pause();
      this.audioBilheteStopped();
    }
  }
  audioBilheteStopped(){
    this.btnAudioBilheteSrc = this.utils.btAudioImg(false); 
    this.isReadingBilhete = false;
  }

  wrongLetter(){
    this.bgLetterSrc = this.utils.imgPath(this.assetPageFolder, "bgLetterWrong.png");
    setTimeout(() => {
      this.bgLetterSrc = this.utils.imgPath(this.assetPageFolder, "bgLetter.png");
     }, 300);
  }

  btnPressed(letter: string){
    this.btnSrc[letter] = this.utils.imgPath(this.assetPageFolder, "BT-" + letter + "-pressionado.png");
    this.smartAudios.play('keyPressed');

    console.log('btnPressed', letter);
    console.log('answers', this.answers);

    switch(this.answers.field){
      case 'voces':
        if (letter === 'V'){
          this.answers.voces = "V";
          this.answers.field = "precisam";
        } else {
          this.wrongLetter();
        }
        break;
      case 'precisam':
        if (letter === 'P'){
          this.answers.precisam = "P";
        this.answers.field = "preocupar1";
        } else {
          this.wrongLetter();
        }
        break;
      case 'preocupar1':
        if (letter === 'P'){
          this.answers.preocupar1 = "P";
          this.answers.field = "preocupar2";
        } else {
          this.wrongLetter();
        }
        break;
      case 'preocupar2':
        if (letter === 'P'){
          this.answers.preocupar2 = "P";
          this.answers.field = "comigo";
        } else {
          this.wrongLetter();
        }
        break;
      case 'comigo':
        if (letter === 'C'){
          this.answers.comigo = "C";
          this.answers.field = "estou";
        } else {
          this.wrongLetter();
        }
        break;
      case 'estou':
        if (letter === 'T'){
          this.answers.estou = "T";
          this.answers.field = "bem";
        } else {
          this.wrongLetter();
        }
        break;
      case 'bem':
        if (letter === 'B'){
          this.answers.bem = "B";
          this.answers.field = "fui";
        } else {
          this.wrongLetter();
        }
        break;
      case 'fui':
        if (letter === 'F'){
          this.answers.fui = "F";
          this.answers.field = "encontrar1";
        } else {
          this.wrongLetter();
        }
        break;
      case 'encontrar1':
        if (letter === 'C'){
          this.answers.encontrar1 = "C";
          this.answers.field = "encontrar2";
        }
        break;
      case 'encontrar2':
        if (letter === 'T'){
          this.answers.encontrar2 = "T";
          this.answers.field = "grande1";
        } else {
          this.wrongLetter();
        }
        break;
      case 'grande1':
        if (letter === 'G'){
          this.answers.grande1 = "G";
          this.answers.field = "grande2";
        } else {
          this.wrongLetter();
        }
        break;
      case 'grande2':
        if (letter === 'D'){
          this.answers.grande2 = "D";
          this.answers.field = "bumba";
        }
        break;
      case 'bumba':
        if (letter === 'B'){
          this.answers.bumba = "B";
          this.answers.field = "boi";
        } else {
          this.wrongLetter();
        }
        break;
      case 'boi':
        if (letter === 'B'){
          this.answers.boi = "B";
          this.answers.field = "brasil";
        } else {
          this.wrongLetter();
        }
        break;
      case 'brasil':
        if (letter === 'B'){
          this.answers.brasil = "B";
          this.answers.field = "ok";

          this.animaNextPageSrc = this.utils.bgNextPage();
        } else {
          this.wrongLetter();
        }
        break;
    }
    
  }

  btnReleased(letter: string){
    this.btnSrc[letter] = this.utils.imgPath(this.assetPageFolder, "BT-" + letter + ".png");
  }

}
