import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page10 } from '../hcPage10/hcPage10';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-hc9',
  templateUrl: 'hcPage9.html'
})
export class Page9 {
  @ViewChild("page9Reading") page9Reading;
  
  private assetPageFolder = "cena_aviao/";
  private animaNextPageSrc: string = ""; 
  private animaAviaoSrc: string = "";
  private btnAudioSrc: string = "";
  private isReading: boolean = false;

  constructor(
    private navCtrl: NavController, 
    private smartAudios: SmartAudioProvider, 
    private utils: UtilsProvider) {
    
  }

  ionViewWillEnter(){
    this.animaNextPageSrc = null;
    this.animaAviaoSrc = this.utils.imgPath(this.assetPageFolder, "aviao.gif");
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.audioStopped();
  }

  ionViewDidEnter(){
    this.smartAudios.loadAndPlay('aviao', 'aviao.mp3', false, false);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 500);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('aviao');
    this.page9Reading.nativeElement.pause();
    this.page9Reading.nativeElement.currentTime = 0;
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, Page10);
  }

  goToBackPage() { 
    this.utils.goToPreviousPage(this.navCtrl); 
  }

  audioReading(){
    // console.log('Reading Audio');
    if (!this.isReading){
      // console.log('playing', this.page9Reading);
      this.page9Reading.nativeElement.play();
      this.btnAudioSrc = this.utils.btAudioImg(true); 
      this.isReading = true;
      this.page9Reading.nativeElement.onended = () => {
        this.audioStopped();
      }
    } else {
      // console.log('paused');
      this.page9Reading.nativeElement.pause();
      this.audioStopped();
    }
  }

  audioStopped(){
    this.btnAudioSrc = this.utils.btAudioImg(false); 
    this.isReading = false;
  }

}
