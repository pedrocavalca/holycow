import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomeIndexPage } from '../homeIndex/homeIndex';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private animaNextPageSrc: string = ""; 

  constructor(
    private  navCtrl: NavController, 
    private  smartAudios: SmartAudioProvider, 
    private  utils: UtilsProvider) {

    smartAudios.preload('turnPage', 'virada-pagina.mp3', false, false);
  }

  ionViewWillEnter() {
    this.animaNextPageSrc = null;
  }

  ionViewDidEnter(){
    this.smartAudios.loadAndPlay('themeMusic', 'musica-tema.mp3', true, true);

    setTimeout(() => {
      this.animaNextPageSrc = this.utils.bgNextPage();
     }, 1000);
  }

  ionViewWillLeave() {
    this.smartAudios.unload('themeMusic');
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.utils.goToNextPage(this.navCtrl, HomeIndexPage);
  }

}
