import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
// import { File } from '@ionic-native/file';
// import { FileOpener } from '@ionic-native/file-opener';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Page1 } from '../hcPage1/hcPage1';
import { Credits } from '../credits/credits';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-homeIndex',
  templateUrl: 'homeIndex.html'
})
export class HomeIndexPage {
  
  constructor(
    private navCtrl: NavController, 
    private platform: Platform,
    // private file: File,
    // private fileOpener: FileOpener,
    private iab: InAppBrowser,
    private smartAudios: SmartAudioProvider,
    private utils: UtilsProvider) {

  }

  ionViewWillEnter() {
    // console.log('will enter homeIndex');
    this.smartAudios.preload('clickButton', 'clicar.mp3', false, false);
  }

  ionViewDidLeave() {
    // console.log('will leave homeIndex');
    this.smartAudios.unload('clickButton');
  }

  goToNextPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.smartAudios.play('clickButton');
    this.utils.goToNextPage(this.navCtrl, Page1);
  }

  goToCreditsPage() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.smartAudios.play('clickButton');
    this.utils.goToNextPage(this.navCtrl, Credits);
  }

  openPdf() {

    this.smartAudios.play('clickButton');
    let url = "http://www.cenpec.org.br/especiais/campanha-global-giving/e-book/livro-vaca-sagrada/";
    let target = "_system";
    this.iab.create(url,target);

    // this.smartAudios.play('clickButton');
    // let pdfFileName: string = 'story.pdf';
    // let rootPdfPath: string = this.file.applicationDirectory + 'www/assets/pdf/';
    // if (this.platform.is('android')){
    //   this.file.copyFile(rootPdfPath, 'story.pdf', this.file.externalDataDirectory, pdfFileName)
    //     .then(onfulfilled => {
    //       let newPdfPath: string = this.file.externalDataDirectory + pdfFileName;
    //       this.fileOpener.open(newPdfPath, 'application/pdf')
    //         .then(() => console.log('File is opened: ' + newPdfPath))
    //         .catch(e => console.log('Error openening file: ' + newPdfPath, e));

    //     }).catch(e => console.log('Error copying file: ', e)
    //   );
    // }
    // //TODO: ios open pdf
    // else {
    //   let pdfPathAndFileName: string = rootPdfPath + pdfFileName;
    //   this.fileOpener.open(pdfPathAndFileName, 'application/pdf')
    //     .then(() => console.log('File is opened: ' + pdfPathAndFileName))
    //     .catch(e => console.log('Error openening file: ' + pdfPathAndFileName, e));
    // }
  }

}
