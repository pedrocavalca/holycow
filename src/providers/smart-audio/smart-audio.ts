import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
// import { NativeAudio } from '@ionic-native/native-audio';

/*
  Generated class for the SmartAudioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SmartAudioProvider {

  audioType: string = 'html5';
  assetsPath: string = 'assets/audio/';
  sounds: any = [];
  audioPlaying: any = null;
  audioReading: any = null;

  constructor(//private nativeAudio: NativeAudio, 
    private platform: Platform) {
    // console.log('Hello SmartAudioProvider Provider');

    // if(platform.is('cordova')){
    //   this.audioType = 'native';
    //   // console.log('this is a native app');
    // }
  }

  preload(key, assetName, isLooping, isComplex) {
 
    let audio = {
        key: key,
        asset: this.assetsPath + assetName,
        isLooping: isLooping,
        isComplex: isComplex,
        isPlaying: false
    };
    this.sounds.push(audio);
    console.log('loading audio', audio);

    // if (this.audioType === 'native'){
    //   if (isComplex){
    //     this.nativeAudio.preloadComplex(key, audio.asset, 1, 1, 0).then(this.onSuccess, this.onError);
    //   } else {
    //     this.nativeAudio.preloadSimple(key, audio.asset).then(this.onSuccess, this.onError);
    //   }
    // }
  }

  loadAndPlay(key, assetName, isLooping, isComplex) {
 
    let audio = {
        key: key,
        asset: this.assetsPath + assetName,
        isLooping: isLooping,
        isComplex: isComplex,
        isPlaying: false
    };
    this.sounds.push(audio);
    // console.log('loading audio', audio);

    // if (this.audioType === 'native'){
    //   const that = this;
    //   if (isComplex){
    //     this.nativeAudio.preloadComplex(key, audio.asset, 1, 1, 0).then(
    //       function(sucess) {
    //         that.play(key);
    //         that.onSuccess(sucess);
    //       }, function(error) {
    //         that.onError(error);
    //       }
    //     );
    //   } else {
    //     this.nativeAudio.preloadSimple(key, audio.asset).then(
    //       function(sucess) {
    //         that.play(key);
    //         that.onSuccess(sucess);
    //       }, function(error) {
    //         that.onError(error);
    //       }
    //     );
    //   }
    // } else {
      this.play(key);
    // }
  }

  unload(key){
    let audio = this.sounds.find((sound) => {
      return sound.key === key;
    });
    if (audio){
      console.log('unloading audio', audio);
      // if (this.audioType === 'native'){
      //   this.nativeAudio.stop(audio.key).then(this.onSuccess, this.onError);
      //   this.nativeAudio.unload(audio.key).then(this.onSuccess, this.onError);
      // } else {
        if (audio.instance){
          audio.instance.pause();
        }
      // }
      let index = this.sounds.indexOf(audio);
      if(index>=0){
        this.sounds.splice(index, 1);
      }
    }
  }

  play(key){
 
    let audio = this.sounds.find((sound) => {
        return sound.key === key;
    });
    if (audio){
      console.log('audio to play', audio);
      // if (this.audioType === 'native') {
      //   if (audio.isComplex){
      //     if (audio.isLooping){
      //       this.nativeAudio.loop(audio.key).then(this.onSuccess, this.onError);
      //     } else {
      //       if (!audio.isPlaying){
      //         audio.isPlaying = true;
      //         this.audioPlaying = audio;
      //         this.nativeAudio.play(audio.key, () => {
      //           // console.log('complex audio is done playing', this.audioPlaying)
      //           this.audioPlaying.isPlaying = false;
      //           this.audioPlaying = null;
      //         }).then(this.onSuccess, this.onError);
      //       } else {
      //         audio.isPlaying = false;
      //         this.audioPlaying = null;
      //         this.nativeAudio.stop(audio.key).then(this.onSuccess, this.onError);
      //       }
      //     }
      //   } else {
      //     this.nativeAudio.play(audio.key, () => {
      //       // console.log('simple audio is done playing')
      //     }).then(this.onSuccess, this.onError);
      //   }
      // } else {
        let audioAsset = new Audio(audio.asset);
        audio.instance = audioAsset;
        audioAsset.loop = audio.isLooping;
        if (audio.isLooping){
          audioAsset.play();
        } else {
          if (audio.instance.duration > 0 && !audio.instance.paused){
            // audio.isPlaying = false;
            audioAsset.pause();
          } else {
            // audio.isPlaying = true;
            audioAsset.play();
          }
        }
      // }
    }
  }

  playReading(key){
    let audio = this.sounds.find((sound) => {
      return sound.key === key;
    });
    // console.log('play reading', audio);
    if (audio){
      // if (this.audioType === 'native') {
      //   if (!audio.isPlaying){
      //     // console.log('audio playing', audio);
      //     audio.isPlaying = true;
      //     this.audioReading = audio;
      //     // console.log('audioReading', audio);
      //     this.nativeAudio.play(audio.key, () => {
      //       this.audioReading.isPlaying = false;
      //       this.audioReading = null;
      //       // console.log('reading audio is done playing', this.audioReading);
      //     }).then(this.onSuccess, this.onError);
      //   } 
      //   // else {
      //   //   console.log('audio stopping', audio);
      //   //   audio.isPlaying = false;
      //   //   this.audioReading = null;
      //   //   this.nativeAudio.stop(audio.key).then(this.onSuccess, this.onError);
      //   // }
      // } else {
        if (audio.instance && audio.instance.duration > 0 && !audio.instance.paused){
          // audio.isPlaying = false;
          audio.instance.pause();
        } else {
          if (audio.instance && audio.duration > 0){
            audio.instance.play();
          } else {
            let audioAsset = new Audio(audio.asset);
            audio.instance = audioAsset;
            audioAsset.loop = audio.isLooping;
            audioAsset.play();
          }
        }
      // }
    }
  }

  stop(key){
    let audio = this.sounds.find((sound) => {
      return sound.key === key;
    });
    this.audioPlaying = null;
    this.audioReading = null;
    audio.isPlaying = false;
    // if (this.audioType === 'native') {
    //   this.nativeAudio.stop(audio.key).then(this.onSuccess, this.onError);
    // } else {
      if (audio.instance){
        audio.instance.pause();
      }
    // }
  }

  onSuccess(res){
    // console.log("success smart-audio: ", res);
  }

  onError(err){
    // console.log("Error smart-audio: ", err);
  }
}
