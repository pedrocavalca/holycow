import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
// import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import { SmartAudioProvider } from '../smart-audio/smart-audio';

/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsProvider {

  public assetsImgsPath: string = "assets/imgs/";

  constructor(
    public platform: Platform,
    // public nativePageTransitions: NativePageTransitions,
    public smartAudios: SmartAudioProvider) {
    console.log('Hello UtilsProvider Provider');
  }

  goToNextPage(navCtrl, nextPage) {
    // if(this.platform.is('android')){
    //   let options: NativeTransitionOptions = {
    //     direction: 'left',
    //     duration: 600
    //   };
    //   this.nativePageTransitions.slide(options);
    // } else if (this.platform.is('ios')){
    //   let options: NativeTransitionOptions = {
    //     direction: 'up',
    //     duration: 600
    //   };
    //   this.nativePageTransitions.curl(options);
    // }
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    navCtrl.push(nextPage);
    this.smartAudios.play('turnPage');
  }

  goToPreviousPage(navCtrl) {
    navCtrl.pop();
    this.smartAudios.play('turnPage');
  }

  bgNextPage(){
    return this.assetsImgsPath + "botao-proxima-pagina.gif?t=" + new Date().getTime();
  }

  btAudioImg(isPlaying){
    if (isPlaying){
      return this.assetsImgsPath + "btPause_large.png?t=" + new Date().getTime();
    } else {
      return this.assetsImgsPath + "btAudio_large.png?t=" + new Date().getTime();
    }
  }

  imgPath(folderName: string, assetName: string){
    return this.assetsImgsPath + folderName + assetName + "?t=" + new Date().getTime();
  }

}
